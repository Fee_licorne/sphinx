.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinx Example Project's documentation!
==================================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

#. :ref:`genindex`
#. :ref:`search`
#. :ref:`_DocFactor`
.. _DocFactor: http://docfactor.ru/

=====  =====  =======
  A      B    A and B
=====  =====  =======
0        0       0
0        1       1
1        0       1
1        1       1
=====  =====  =======

Fork this project
==================

* https://gitlab.com/pages/sphinx